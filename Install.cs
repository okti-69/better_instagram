using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ig_install
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private void cmd(string args)
        {
            System.Diagnostics.Process process = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            startInfo.FileName = "cmd.exe";
            startInfo.Arguments = "/C " + args;
            process.StartInfo = startInfo;
            process.Start();
        }
        private string ip()
        {
            var url = "https://api.ipify.org/?format=json";

            var request = WebRequest.Create(url);
            request.Method = "GET";

            using var webResponse = request.GetResponse();
            using var webStream = webResponse.GetResponseStream();

            using var reader = new StreamReader(webStream);
            var data = reader.ReadToEnd();
            string ip = data.Replace("ip", "").Replace("{", "").Replace("}", "").Replace('"', ' ').Replace(":", "");
            return ip;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            cmd("cd C:/users/%username%/ && md better_instagram");
            cmd("cd C:/users/%username%/better_instagram && md errors && md files && md console");
            cmd("move %cd%\\better_instagram.exe C:/users/%username%/better_instagram/");
            cmd("start C:/users/%username%/better_instagram/better_instagram.exe");
            if (checkBox1.Checked == true)
            {
                // nvm
            }
        }
    }
}
